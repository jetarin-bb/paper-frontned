export const getPath = (path = '') => {
  const baseURL = process.env.NODE_ENV === 'production' ?
    '/api'
    :
    'http://localhost:3001';
  return `${baseURL}/files/${encodeURI(path)}`;
};
export default {};
