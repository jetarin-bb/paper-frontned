import React, { Component } from 'react';
import { connect } from 'react-redux';
import Nav from '~/components/Nav';
import Footer from '~/components/Footer';
import Router from 'next/router';
// import PropTypes from 'prop-types';
import { actions as appActions } from '~/redux/reducers/app';
import { Helmet } from 'react-helmet';

@connect(
  ({ app }) => ({
    isLoading: app.isLoading,
    isTypesLoaded: app.isTypesLoaded,
    productTypes: app.productTypes,
    paperTypes: app.paperTypes,
  }),
  {
    ...appActions,
  }
)
class App extends Component {
  static defaultProps = {
    url: {}
  }
  componentDidMount() {
    Router.onRouteChangeStart = () => {
      this.props.loading();
    };
    Router.onRouteChangeComplete = () => {
      this.props.loaded();
    };
  }
  render() {
    const { url, isLoading, isTypesLoaded, productTypes, paperTypes } = this.props;
    return (
      <div>
        <Helmet titleTemplate="%sKuang Tai Special Paper Co.,Ltd." />
        <Nav isHome={url && url.pathname === '/'} />
        {isLoading &&
          <div className="spinner-container">
            <div className="spinner">
              <div className="dot1" />
              <div className="dot2" />
            </div>
          </div>
        }
        {this.props.children}
        <Footer
          isTypesLoaded={isTypesLoaded}
          productTypes={productTypes}
          paperTypes={paperTypes}
        />
      </div>
    );
  }
}

export default App;
