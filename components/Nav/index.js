import React, { Component } from 'react';
import Link from 'next/link';
import { connect } from 'react-redux';
import styled from 'styled-components';
import colors from '~/theme/colors';
import { actions as productActions } from '~/redux/reducers/product';
import { actions as paperActions } from '~/redux/reducers/paper';
import { actions as appActions } from '~/redux/reducers/app';

const Banner = styled.div`
  background-color: ${colors.cyan};
  background-image: url('/static/images/home/kt-banner.jpg');
  background-size: cover;
  background-position: 50%;
  background-repeat: no-repeat;
`;

const BannerInner = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  min-height: 467px;
  padding: 20px 0;
  position: relative;
  box-sizing: border-box;
  @media (min-width: 701px) {
    padding-left: 148px;
  }
  @media (max-width: 700px) {
    min-height: 330px;
    padding-top: 50px;
  }
  >h1 {
    position: relative;
    z-index: 30;
    margin-bottom: 10px;
    @media (max-width: 700px) {
      text-align: center;
      margin-top: 90px;
    }
  }
  >h3 {
    position: relative;
    z-index: 30;
    margin-top: 10px;
    @media (max-width: 700px) {
      text-align: center;
    }
  }
`;

const Logo = styled.div`
  position: relative;
  z-index: 40;
  display: flex;
  align-items: center;
  >div {
    display: inline-block;
    background-image: url(${'/static/images/icon.png'});
    background-size: cover;
    background-position: 50%;
    background-repeat: no-repeat;
    width: 76px;
    height: 76px;
    margin-right: 10px;
  }
  >p {
    margin: 0;
    font-weight: 500;
    >span {
      color: ${colors.white};
    }
  }
  @media (max-width: 700px) {
    display: none !important;
  }
`;

const BannerImage = styled.div`
  width: 600px;
  height: 467px;
  position: absolute;
  z-index: 20;
  right: 0;
  background-color: ${colors.green};
  top: 0;
  @media (max-width: 700px) {
    height: 110px;
    bottom: 0;
    width: 100%;
    top: initial;
  }
`;

const NavBar = styled.div`
  background-color: ${colors.white};
  box-shadow: 0 1px 2px 0 rgba(148, 148, 148, 0.5);
  height: 90px;
  @media (max-width: 700px) {
    height: 50px;
    position: fixed;
    z-index: 40;
    top: 0;
    left: 0;
    width: 100%;
  }
`;

const NavIcon = styled.div`
  position: absolute;
  width: 27px;
  height: 27px;
  top: 12px;
  left: 0;
  right: 0;
  margin: auto;
  @media (min-width: 701px) {
    display: ${({ isHome }) => (isHome ? 'none !important' : 'block')};
    left: ${({ isHome }) => (isHome ? '0' : '145px')};
    right: initial;
    top: 22.5px;
    width: 140px;
    height: 45px;
  }
  >div {
    display: inline-block;
    background-image: url(${'/static/images/icon.png'});
    background-size: cover;
    background-position: 50%;
    background-repeat: no-repeat;
    width: 27px;
    height: 27px;
    @media (min-width: 701px) {
      width: 45px;
      height: 45px;
    }
  }
  >span {
    font-size: 12px;
    position: relative;
    top: -10px;
    @media (min-width: 701px) {
      font-size: 13px;
      position: relative;
      top: -17px;
    }
  }
`;

const Burger = styled.div`
  cursor: pointer;
  @media (min-width: 701px) {
    display: none !important;
  }
  box-sizing: border-box;
  padding: 17px 10px;
  width: 40px;
  >div {
    width: 20px;
    height: 2px;
    background-color: ${colors.green};
    &:not(:last-child) {
      margin-bottom: 5px;
    }
  }
`;
const MobileTitle = styled.li`
  @media (min-width: 701px) {
    display: none !important;
  }
  >p {
    text-align: center !important;
    font-weight: 500;
    margin: 20px 0 10px;
    font-size: 24px;
    color: ${colors.gray};
  }
`;

const CloseButton = styled.div`
  @media (min-width: 701px) {
    display: none !important;
  }
  width: 28px;
  height: 28px;
  box-sizing: border-box;
  border-radius: 50%;
  border: 1px solid ${colors.lineGray};
  background-color: ${colors.white};
  position: absolute;
  top: -14px;
  right: -2px;
  cursor: pointer;
  >div {
    width: 20px;
    height: 2px;
    background-color: ${colors.lineGray};
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    &:nth-child(1) {
      transform: rotate(45deg);
    }
    &:nth-child(2) {
      transform: rotate(-45deg);
    }
  }
`;

const NavMenu = styled.div`
  align-item: center;
  max-width: 1280px;
  margin: 0 auto;
  >ul {
    height: 90px;
    margin: 0;
    padding: ${({ isHome }) => (isHome ? '0 148px' : '0 148px 0 288px')};
    justify-content: space-between;
    display: flex;
    list-style-type: none;
    >li {
      font-size: 24px;
      display: inline-block;
      >a {
        height: 90px;
        display: block;
        padding: 30px 0;
        box-sizing: border-box;
        color: ${colors.gray};
        &:hover {
          color: ${colors.green};
        }
      }
    }
  }
  @media (max-width: 700px) {
    ${({ active }) => (active ?
    `
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    `
    : ''
  )}
    >ul {
      ${({ active }) => (active ? '' : 'display: none;')}
      height: auto;
      padding: 0 10px;
      box-sizing: border-box;
      list-style-type: none;
      flex-direction: column;
      box-shadow: 1px 2px 4px 0 rgba(148,148,148,0.4);
      background-color: ${colors.white};
      width: 280px;
      height: 330px;
      border-radius: 3px;
      box-shadow: 1px 0px 6px 3px rgba(0, 0, 0, .3);
      >li {
        >a {
          width: 260px;
          text-align: center;
          padding: 10px 0;
          height: auto;
          font-size: 18px;
          border-bottom: 2px solid ${colors.lineGray};
        }
      }
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
      justify-content: flex-start;
    }
  }
`;

const SubMenu = styled.li`
  @media (min-width: 701px) {
    position: relative;
    &:hover {
      >ul {
        visibility: visible;
        opacity: 1;
        transform: translateY(0.5%);
        transition-delay: 0s;
      }
    }
  }
  >ul {
    @media (max-width: 700px) {
      display: none;
    }
    @media (min-width: 701px) {
      background-color: ${colors.white};
      position: absolute;
      top: 100%;
      left: 0;
      padding: 0;
      margin-top: 0;
      opacity: 0;
      visibility: hidden;
      border-bottom-left-radius: 5px;
      border-bottom-right-radius: 5px;
      background-color: $white;
      border: 1px solid #d8d8d8;
      border-top: 0;
      transform: translateZ(0);
      transform: translateY(-10%);
      transition: all .5s ease 0s,visibility 0s linear .5s;
      z-index: 105;
      >li {
        float: none;
        min-width: 200px;
        border-bottom: 1px solid #d8d8d8;
        margin: 0;
        list-style: none;
        font-size: 18px;
        >a {
          display: block;
          height: 40px;
          box-sizing: border-box;
          padding: 8px 10px;
          width: 100%;
          color: ${colors.lightGray};
          white-space: nowrap;
          &:hover {
            color: ${colors.green};
          }
        }
      }
    }
  }
`;

const NavContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media (max-width: 700px) {
    flex-direction: column-reverse;
  }
`;
const Arrow = styled.span`
  font-size: 14px;
  position: relative;
  top: -2px;
  @media (max-width: 700px) {
    display: none;
  }
`;
@connect(
  ({ app }) => ({
    isTypesLoaded: app.isTypesLoaded,
    productTypes: app.productTypes,
    paperTypes: app.paperTypes,
  }),
  {
    ...productActions,
    ...paperActions,
    ...appActions,
  }
)
export default class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }
  async componentDidMount() {
    if (!this.props.isTypesLoaded) {
      const types = await Promise.all([
        this.props.getProductTypes(),
        this.props.getPaperTypes()
      ]);
      this.props.setTypes({
        productTypes: types[0].data.map(t => t.name),
        paperTypes: types[1].data.map(t => t.name),
      });
    }
  }
  render() {
    const { active } = this.state;
    const { isHome, paperTypes, productTypes } = this.props;
    return (
      <NavContainer>
        {isHome &&
          <Banner>
            <BannerInner>
              <Link href="/">
                <Logo>
                  <div />
                  <p>
                    <span>Kuang Tai Special Paper Co.,Ltd.</span>
                  </p>
                </Logo>
              </Link>
              <h1>Less is more</h1>
              <h3>Mulberry Paper<br/>Design of Everything</h3>
              {/*
                <BannerImage />
              */}
            </BannerInner>
          </Banner>
        }
        <NavBar>
          <div style={{ maxWidth: '1280px', margin: '0 auto', position: 'relative'}}>
            <Burger onClick={() => this.setState({ active: !active })}>
              <div />
              <div />
              <div />
            </Burger>
            <NavIcon isHome={isHome}>
              <div />
              {/* <span>Kuang Tai Special Paper Co.,Ltd.</span> */}
            </NavIcon>
            <NavMenu active={active} isHome={isHome} onClick={() => this.setState({ active: false })}>
              <ul
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                }}
              >
                <CloseButton onClick={() => this.setState({ active: false })}>
                  <div/>
                  <div/>
                </CloseButton>
                <MobileTitle><p>MENU</p></MobileTitle>
                <li onClick={() => this.setState({ active: false })}>
                  <Link href="/"><a>Home</a></Link>
                </li>
                <SubMenu onClick={() => this.setState({ active: false })}>
                  <Link href="/paper">
                    <a>Paper <Arrow>&#9660;</Arrow></a>
                  </Link>
                  <ul>
                    {paperTypes.map(t => (
                      <li key={t}><Link href={`/paper?type=${t}`}><a>{t}</a></Link></li>
                    ))}
                  </ul>
                </SubMenu>
                <SubMenu onClick={() => this.setState({ active: false })}>
                  <Link href="/products">
                    <a>Product <Arrow>&#9660;</Arrow></a>
                  </Link>
                  <ul>
                    {productTypes.map(t => (
                      <li key={t}><Link href={`/products?type=${t}`}><a>{t}</a></Link></li>
                    ))}
                  </ul>
                </SubMenu>
                <li onClick={() => this.setState({ active: false })}>
                  <Link href="/about-us"><a>About us</a></Link>
                </li>
                <li onClick={() => this.setState({ active: false })}>
                  <Link href="/contact-us"><a>Contact us</a></Link>
                </li>
              </ul>
            </NavMenu>
          </div>
        </NavBar>
      </NavContainer>
    );
  }
}
