import React, { Component } from 'react';

import colors from '~/theme/colors';
import styled from 'styled-components';
import { getPath } from '~/utils/media';

const MainContainer = styled.div`
  padding-right: 30px;
  @media (max-width: 700px) {
    padding-right: 0;
    padding-bottom: 20px;
  }
`;

const ActiveImageContainer = styled.div`
  cursor: pointer;
  height: 500px;
  overflow: hidden;
  width: 100%;
  box-shadow: 0 1px 2px 0 rgba(206, 206, 206, 0.5);
  @media (max-width: 700px) {
    height: 200px;
  }
  position: relative;
  >img {
    width: 100%;
    height: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
  }
`;

const SliderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 20px;
`;

const Chevron = styled.div`
  cursor: pointer;
  min-width: 30px;
  min-height: 30px;
  font-size: 36px;
  text-align: ${({ direction }) => direction};
  color: ${colors.gray};
  user-select: none;
  visibility: ${({ isHidden }) => (isHidden ? 'hidden' : 'visible')};
  @media (max-width: 700px) {
    font-size: 28px;
    min-width: 24px;
  }
`;

const Slider = styled.div`
  flex: 1;
  width: 100%;
`;
const OverFlowSlider = styled.div`
  overflow: hidden;
`;
const ScrollSlider = styled.div`
  transform: translateX(${({ x }) => x * -33.33}%);
  white-space: nowrap !important;
  overflow-x: visible;
  transition: transform .3s;
  padding: 3px 0;
`;
const Thumbnail = styled.div`
  padding: 0 5px;
  width: 33.33%;
  user-select: none;
  display: inline-block !important;
  vertical-align: top !important;
  white-space: normal !important;
  box-sizing: border-box;
  >div {
    width: 100%;
    padding-bottom: 100%;
    position: relative;
    overflow: hidden; 
    cursor: pointer;
    box-shadow: ${({ isActive }) => (isActive ? '0 0 4px 0 rgba(190, 190, 190, 0.8)' : 'none')};
    >img {
      display: block;
      height: auto;
      width: 100%;
      position: absolute;
      z-index: 1;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
    }
  }
`;

export default class ProductFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeImage: 0,
      scrollX: 0,
    };
  }
  go(direction = 'left') {
    const { scrollX } = this.state;
    const { images } = this.props;
    if (direction === 'left' && scrollX === 0) return;
    if (direction === 'right' && scrollX + 3 >= images.length) return;
    let add = -1;
    if (direction === 'right') add = 1;
    this.setState({ scrollX: (add + scrollX) % images.length });
  }

  render() {
    const { images } = this.props;
    const { activeImage, scrollX } = this.state;
    return (
      <MainContainer>
        <ActiveImageContainer>
          {images && images.length > activeImage &&
            <img src={getPath(images[activeImage])} key={`image-${images[activeImage]}`} alt={images[activeImage]} />
          }
        </ActiveImageContainer>
        {images && images.length > 0 &&
          <SliderContainer>
            <Chevron direction="left" onClick={() => this.go('left')} isHidden={scrollX === 0}>
              <span>&lt;</span>
            </Chevron>
            <Slider>
              <OverFlowSlider>
                <ScrollSlider x={scrollX} >
                  {images.map((image, index) => (
                    <Thumbnail key={image} isActive={activeImage === index}>
                      <div onClick={() => this.setState({ activeImage: index })}>
                        <img src={getPath(image)} alt={image} />
                      </div>
                    </Thumbnail>
                  ))}
                </ScrollSlider>
              </OverFlowSlider>
            </Slider>
            <Chevron direction="right" onClick={() => this.go('right')} isHidden={scrollX + 3 >= images.length}>
              <span>&gt;</span>
            </Chevron>
          </SliderContainer>
        }
      </MainContainer>
    );
  }
}
