import React from 'react';

import colors from '~/theme/colors';
import styled from 'styled-components';
import Gallery from '~/components/ProductDetail/Gallery';
import { Link as ScrollLink } from 'react-scroll'

const MainContainer = styled.div`
  display: flex;
  margin: 30px 0;
  @media (max-width: 700px) {
    flex-direction: column;
    margin: 20px 0; 
  }
  padding: 0 0 20px;
  border-bottom: 1px solid ${colors.lineGray};
`;

const GalleryContainer = styled.div`
  min-width: 50%;
  max-width: 50%;
  @media (max-width: 700px) {
    min-width: 100%;
    max-width: 100%;
  }
`;

const ContentContainer = styled.div`
  min-width: 50%;
  max-width: 50%;
  @media (max-width: 700px) {
    min-width: 100%;
    max-width: 100%;
  }
`;

const Type = styled.h4`
  color: ${colors.lightGray};
  margin: 0 0 10px;
`;
const Header = styled.h1`
  font-size: 36px;
  margin: 10px 0;
  @media (max-width: 700px) {
    font-size: 24px;
  }
`;
const Detail = styled.p`
  color: ${colors.lightGray};
  margin: 10px 0 20px;
  font-size: 15px;
  >span {
    white-space: pre-line;
  }
  @media (max-width: 700px) {
    font-size: 13px;
  }
`;
const Colors = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  >h4 {
    margin: 0 10px 0 0;
    font-size: 18px;
  }
  @media (max-width: 700px) {
    >h4 {
      font-size: 15px;
      margin: 0 0 10px 0;
      width: 100%;
    }
  }
`;
const Color = styled.div.attrs({
  title: ({ name }) => name || 'Color',
})`
  min-width: 40px;
  max-width: 40px;
  min-height: 40px;
  max-height: 40px;
  background-color: ${({ color }) => color || colors.lightGray};
  margin-right: 10px;
`;

const OrderButton = styled.div`
  font-size: 24px;
  height: 65px;
  width: 200px;
  background-color: ${colors.green};
  color: ${colors.white};
  font-weight: 500;
  display: block;
  text-align: center;
  box-sizing: border-box;
  padding: 16px;
  margin: 20px 0 0;
  cursor: pointer;
  @media (max-width: 700px) {
    height: 45px;
    padding: 10px;
    width: 180px;
    font-size: 18px;
    margin: 20px auto 0;
  }
`;

const ProductInfo = (props) => {
  const { type, name, spec, description, availableColors, images } = props;
  return (
    <MainContainer>
      <GalleryContainer>
        <Gallery images={images} />
      </GalleryContainer>
      <ContentContainer>
        <Type>{type}</Type>
        <Header>{name}</Header>
        <Detail>
          <span>{description}</span>
          {spec && <br/>}
          {spec && <br/>}
          {spec && <span>{spec}</span>}
        </Detail>
        {availableColors && availableColors.length > 0 &&
          <Colors>
            <h4>Available colors: </h4>
            {availableColors.map((c, i) => <Color name={c.name} color={c.color} key={`color-${i}`} />)}
          </Colors>
        }
        <ScrollLink
          to="order"
          smooth
          offset={-60}
          duration={500}
        >
          <OrderButton>Order</OrderButton>
        </ScrollLink>
      </ContentContainer>
    </MainContainer>
  );
}

export default ProductInfo;
