import React from 'react';

import ProductBox from '~/components/Product/ProductBox';
import colors from '~/theme/colors';
import styled from 'styled-components';
import product from '../../redux/reducers/product';

const Header = styled.h2`
  margin: 30px 0 20px;
  font-size: 36px;
  font-weight: 500;
  @media (max-width: 700px) {
    font-size: 18px;
    margin: 20px 0 12px;
  }
`;

const ProductsContainer = styled.div`
  margin: 20px 0;
  &::after{
    content: " ";
    display: block; 
    height: 0; 
    clear: both;
  }
  border-bottom: 1px solid ${colors.lineGray};
`;

const ProductBoxContainer = styled.div`
  float: left;
  width: 33.33%;
  box-sizing: border-box;
  @media (min-width: 701px) {
    &:nth-child(3n+1) {
      padding-right: 13.34px;
      clear: both;
    }
    &:nth-child(3n+2) {
      padding-left: 6.66px;
      padding-right: 6.66px;
    }
    &:nth-child(3n+3) {
      padding-left: 13.34px;
    }
    margin-bottom: 20px;
  }
  @media (max-width: 700px) {
  ${({ view }) => (view === 'box' ?
    `
      width: 50%;
      &:nth-child(2n+1) {
        padding-right: 5px;
        clear: both;
      }
      &:nth-child(2n+2) {
        padding-left: 5px;
      }
    `
    :
    `
      width: 100%;
      clear: both;
      &:not(:last-child) {
        border-bottom: 1px solid ${colors.lineGray};
      }
      &:not(:first-child) {
        margin-top: 10px;
        border-bottom: 1px solid ${colors.lineGray};
      }
      padding-bottom: 10px;
    `
  )}
  }
`;

const RelatedProducts = (props) => {
  const { products, path } = props;
  if (!products || products.length < 1) {
    return <div />;
  }
  return (
    <div>
      <Header>Related Products</Header>
      <ProductsContainer>
        {products && products.map((item, index) => (
          <ProductBoxContainer key={`item-${index}`} view="box">
            <ProductBox
              name={item.name}
              description={item.description}
              slug={item.slug}
              type={item.type}
              coverImage={item.cover_image}
              view="box"
              path={path || 'product'}
            />
          </ProductBoxContainer>
        ))}
      </ProductsContainer>
    </div>
  );
}

export default RelatedProducts;
