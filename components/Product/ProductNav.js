import React, { Component } from 'react';
import { Select } from '~/components/General';

import colors from '~/theme/colors';
import styled from 'styled-components';
import _ from 'lodash';

const Container = styled.div`
  user-select: none;
`;
const Showing = styled.span`
  font-size: 15px;
  margin: 0 10px;
  color: ${colors.gray};
`;

const Chevron = styled.span`
  font-size: 30px;
  position: relative;
  cursor: pointer;
  top: 3px;
  color: ${({ active }) => (active ? colors.gray : colors.lightGray)};
  margin: 0 10px;
`;

// @connect(
//   state => ({
//     products: state.product.products,
//     isLoading: state.product.isLoading,
//     count: state.product.count,
//   }),
//   {
//     ...productActions,
//   }
// )
export default class ProductNav extends Component {
  render() {
    const { pages, currentPage } = this.props;
    const options = _.range(1, pages + 1).map(o => ({ label: o, value: o }));
    return (
      <Container>
        <Showing>Page </Showing>
        <Select
          width="90px"
          options={options}
          value={currentPage}
          onChange={e => this.props.onChangePage(parseInt(e.target.value, 10))}
        />
        <Showing> of {pages}</Showing>
        <Chevron
          onClick={() => {
            if (currentPage !== 1) {
              this.props.onChangePage(parseInt(currentPage - 1, 10));
            }
          }}
          active={currentPage !== 1}
        >
          &lt;
        </Chevron>
        <Chevron
          onClick={() => {
            if (currentPage !== pages) {
              this.props.onChangePage(parseInt(currentPage + 1, 10));
            }
          }}
          active={currentPage !== pages}
        >
          &gt;
        </Chevron>
      </Container>
    );
  }
}

