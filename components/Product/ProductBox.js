import React from 'react';
// import Link from 'next/link'

import colors from '~/theme/colors';
import styled from 'styled-components';
import { getPath } from '~/utils/media';

const MainContainer = styled.div`
  @media (max-width: 700px) {
    display: flex;
    flex-direction: ${({ view }) => (view === 'box' ? 'column' : 'row')};
  }
`;
const ImageContainer = styled.div`
  height: 300px;
  box-shadow: 0 1px 2px 0 rgba(206, 206, 206, 0.5);
  padding: 10px;
  box-sizing: border-box;
  background-image: ${({ coverImage }) => `url(${getPath(coverImage)})`};
  background-size: cover;
  background-position: 50%;
  background-repeat: no-repeat;
  @media (max-width: 700px) {
    width: ${({ view }) => (view === 'box' ? '100%' : '140px')};
    min-width: ${({ view }) => (view === 'box' ? 'auto' : '140px')};
    height: 140px;
  }
`;
const ContentContainer = styled.div`
  margin: 20px 0;
  >h4 {
    font-weight: 500;
    color: ${colors.gray};
    margin: 10px 0;
  }
  >p {
    margin: 10px 0;
    color: ${colors.lightGray};
    display: -webkit-box;
    text-overflow: ellipsis;
    -webkit-box-orient: vertical;
    overflow-y: hidden;
    max-height: 92px;
    min-height: 92px;
    -webkit-line-clamp: 4;
  }
  @media (max-width: 700px) {
    margin: ${({ view }) => (view === 'box' ? '0 0 10px' : '10px 0 0 10px')};
    > p {
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
      max-height: 48px;
      overflow-y: hidden;
      min-height: initial
    }
  }
`;
const ViewMore = styled.p`
  color: ${colors.green} !important;
  margin-top: 20px !important;
  text-decoration: underline;
  @media (max-width: 700px) {
    display: none !important;
  }
`;

const ProductBox = (props) => (
  <a href={`/${props.path}/${props.slug}`} target="_blank">
    <MainContainer view={props.view}>
      <ImageContainer coverImage={props.coverImage} view={props.view}/>
      <ContentContainer view={props.view}>
        <h4>
          {props.name}<br/>
          {props.type}
        </h4>
        <p>{props.description}</p>
        <ViewMore>View more</ViewMore>
      </ContentContainer>
    </MainContainer>
  </a>
);

export default ProductBox;
