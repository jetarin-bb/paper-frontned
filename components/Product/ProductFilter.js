import React, { Component } from 'react';
import { Select } from '~/components/General';

import colors from '~/theme/colors';
import styled from 'styled-components';

const Desktop = styled.div`
  margin: 30px 0;
  @media (max-width: 700px) {
    display: none;
  }
  &::after{
    content: " ";
    display: block; 
    height: 0; 
    clear: both;
  }
`;

const FloatLeft = styled.div`
  float: left;
`;
const FloatRight = styled.div`
  float: right;
`;
const Showing = styled.span`
  font-size: 15px;
  margin-left: 20px;
  color: ${colors.gray};
`;

const Mobile = styled.div`
  @media (min-width: 701px) {
    display: none;
  }
  display: flex;
  >select {
    flex: 1;
  }
`;

const ToggleBox = styled.div`
  width: 35px;
  min-width: 35px;
  height: 35px;
  min-height: 35px;
  cursor: pointer;
  border: 1px solid ${colors.borderGray};
  box-sizing: border-box;
  margin-left: 10px;
  background-color: ${({ active, type }) => {
    if (active && type === 'box') {
      return colors.lightGray;
    }
    if (!active && type === 'box') {
      return colors.white;
    }
    if (active && type === 'list') {
      return colors.lightGray;
    }
    if (!active && type === 'list') {
      return colors.white;
    }
    return colors.white;
  }};
  background-image: url('/static/images/products/${props => props.type}.png');
  background-size: 24px;
  background-repeat: no-repeat;
  background-position: 50%;
`;

// @connect(
//   state => ({
//     products: state.product.products,
//     isLoading: state.product.isLoading,
//     count: state.product.count,
//   }),
//   {
//     ...productActions,
//   }
// )
export default class ProductFilter extends Component {
  render() {
    const { view, selectedProductType, productTypes, total, showing } = this.props;
    return (
      <div>
        <Desktop>
          <FloatLeft>
            <Select
              width="300px"
              options={productTypes}
              value={selectedProductType}
              onChange={(e) => this.props.onChangeProductType(e.target.value)}
            />
            <Showing>View {showing} / {total}</Showing>
          </FloatLeft>
          <FloatRight>
            {this.props.renderNav && this.props.renderNav()}
          </FloatRight>
        </Desktop>
        <Mobile>
          <Select
            width="100%"
            options={productTypes}
            value={selectedProductType}
            onChange={(e) => this.props.onChangeProductType(e.target.value)}
          />
          <ToggleBox type="box" active={view === 'box'} onClick={() => this.props.onChangeView('box')}/>
          <ToggleBox type="list" active={view === 'list'} onClick={() => this.props.onChangeView('list')} />
        </Mobile>
      </div>
    );
  }
}

