import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { addCount } from '../store'
import styled, { serverStylesheet } from 'styled-components';

const Header = styled.h1`
  color: red;
  margin: 20px auto;
`;

class AddCount extends Component {
  add = () => {
    this.props.addCount()
  }

  render () {
    // serverStylesheet.reset();
    const { count } = this.props
    return (
      <div>
        <Header>AddCount: <span>{count}</span></Header>
        <button onClick={this.add}>Add To Count</button>
      </div>
    )
  }
}

const mapStateToProps = ({ count }) => ({ count })

const mapDispatchToProps = (dispatch) => {
  return {
    addCount: bindActionCreators(addCount, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCount)
