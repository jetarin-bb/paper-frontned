import React, { Component } from 'react';
import Link from 'next/link'
import styled from 'styled-components';
import colors from '~/theme/colors';

const FooterContainer = styled.div`
  background-color: ${colors.footerGray};
  color: ${colors.white};
`;

const FooterInner = styled.div`
  max-width: 1280px;
  box-sizing: border-box;
  margin: 0 auto;
  margin-top: 30px;
  padding: 40px 148px;
  @media (max-width: 700px) {
    padding: 20px;
  }
  &::after { 
    content: " ";
    display: block; 
    height: 0; 
    clear: both;
  }
`;
const FooterNav = styled.div`
  float: left;
  width: 33.33%;
  box-sizing: border-box;
  font-size: 24px;
  >ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
    >li {
      >a {
        color: ${colors.white};
        font-weight: 500;
      }
      &:not(:last-child) {
        @media (min-width: 701px) {
          margin-bottom: 13px;
        }
      }
    }
  }
  @media (max-width: 700px) {
    width: 100%;
    font-size: 13px;
    >ul {
      display: flex;
      justify-content: center;
      flex-wrap: wrap;
      >li {
        padding: 0 20px;
        margin-bottom: 26px;
      }
    }
  }
`;

const FooterLinks = styled.div`
  @media (max-width: 700px) {
    display: none;
  }
  float: left;
  width: 33.33%;
  >h3 {
    color: ${colors.white};
    font-size: 24px;
    margin: 0 0 20px;
    font-weight: 500;
  }
  >p {
    margin: 20px 0;
    font-size: 18px;
    &:last-child {
      margin-bottom: 0;
    }
    >a {
      color: ${colors.white};
    }
  }
`;

export default class Footer extends Component {
  render() {
    const { isTypesLoaded, paperTypes, productTypes } = this.props;
    return (
      <FooterContainer>
        <FooterInner>
          <FooterNav>
            <ul>
              <li>
                <Link href="/"><a>Home</a></Link>
              </li>
              <li>
                <Link href="/paper"><a>Paper</a></Link>
              </li>
              <li>
                <Link href="/products"><a>Product</a></Link>
              </li>
              <li>
                <Link href="/about-us"><a>About us</a></Link>
              </li>
              <li>
                <Link href="/contact-us"><a>Contact us</a></Link>
              </li>
            </ul>
          </FooterNav>
          <FooterLinks>
            <h3>Mulberry Paper</h3>
            {isTypesLoaded && paperTypes && paperTypes.filter((p, i) => i <= 3).map(paper => (
              <p key={paper}><Link href={`/paper?type=${paper}`}><a>{paper}</a></Link></p>
            ))}
          </FooterLinks>
          <FooterLinks>
            <h3>Catagories</h3>
            {isTypesLoaded && productTypes && productTypes.filter((p, i) => i <= 3).map(prod => (
              <p key={prod}><Link href={`/product?type=${prod}`}><a>{prod}</a></Link></p>
            ))}
          </FooterLinks>
        </FooterInner>
      </FooterContainer>
    );
  }
}
