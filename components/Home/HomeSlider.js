import React, { Component } from 'react';
import styled from 'styled-components';
import colors from '~/theme/colors';
import { getPath } from '~/utils/media';
// import OwlCarousel from 'react-owl-carousel';
import _Slider from 'react-slick';

const MainContainer = styled.div`
  margin: 100px 0;
  @media (max-width: 700px) {
    margin 50px 0;
  }
`;

const Header = styled.h2`
  font-weight: 500;
  @media (max-width: 700px) {
    font-size: 36px;
    text-align: center;
  }
`;

const Slider = styled(_Slider)`
  .paper-dots {
    list-style-type: none;
    padding: 0;
    display: flex !important;
    justify-content: center;
    >li {
      margin: 0 10px;
      >button {
        cursor: pointer;
        border-radius: 50%;
        border: 1px solid #979797;
        color: #d8d8d8;
        background-color: #d8d8d8d;
        min-width: 20px;
        max-width: 20px;
        min-height: 20px;
        max-height: 20px;
        outline: none;
      }
    }
    .slick-active {
      >button {
        background-color: ${colors.green};
        color: ${colors.green};
      }
    }
  }
  @media (max-width: 700px) {
    display: none !important;
  }
`;

const ScrollContainer = styled.div`
  @media (min-width: 701px) {
    display: none !important;
  }
  margin-left: -10px;
  margin-right: -10px;
`;
const ScrollPadding = styled.div`
  position: relative;
`;
const Scroll = styled.div`
  overflow-x: auto;
  overflow-y: hidden;
  white-space: nowrap !important;
`;

const PaperBox = styled.div`
  cursor: pointer;
  >a {
    display: block;
    img {
      display: block;
      height: 260px;
      width: 100%;
      object-fit: cover;
    }
    padding: 0 10px;
    box-sizing: border-box;
    h4 {
      color: ${colors.lightGray};
    }
  }
  @media (max-width: 700px) {
    &:first-child {
      margin-left: 10px;
    }
    margin-right: 10px;
    width: 75%;
    user-select: none;
    display: inline-block !important;
    vertical-align: top !important;
    white-space: normal !important;
    box-sizing: border-box;
  }
`;

export default class HomeSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMounted: false
    };
  }
  componentDidMount() {
    this.setState({ isMounted: true }); // eslint-disable-line
  }
  render() {
    const { isMounted } = this.state;
    const { items } = this.props;
    return (
      <MainContainer>
        <Header>Products</Header>
        {isMounted &&
          <Slider
            dots
            speed={700}
            slidesToShow={3}
            slidesToScroll={3}
            dotsClass="paper-dots"
            swipe
          >
            {items && items.map(item => (
              <PaperBox key={item._id}>
                <a href={`/product/${item.slug}`} target="_blank">
                  <img src={getPath(item.cover_image)} alt={item.name} />
                  <h4>{item.name}</h4>
                </a>
              </PaperBox>
            ))}
          </Slider>
        }
        <ScrollContainer>
          <ScrollPadding>
            <Scroll>
              {items && items.map(item => (
                <PaperBox key={item._id}>
                  <a href={`/product/${item.slug}`} target="_blank">
                    <img src={getPath(item.cover_image)} alt={item.name} />
                    <h4>{item.name}</h4>
                  </a>
                </PaperBox>
              ))}
            </Scroll>
          </ScrollPadding>
        </ScrollContainer>
      </MainContainer>
    );
  }
}
