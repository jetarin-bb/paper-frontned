import React from 'react';
// import Link from 'next/link';
import styled from 'styled-components';
import colors from '~/theme/colors';
import { getPath } from '~/utils/media';

const MainContainer = styled.div`
  margin: 40px 0;
`;

const ItemContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: ${({ isImageFirst }) => (isImageFirst ? 'row' : 'row-reverse')};
  @media (max-width: 700px) {
    flex-direction: column;
  }
`;
const Arrow = styled.span`
  position: relative;
  top: 2px;
  font-size: 24px;
`;

const ImageContaier = styled.div`
  max-width: 48%;
  min-width: 48%;
  @media (max-width: 700px) {
    max-width: 100%;
    min-width: 100%;
  }
`;
const ItemContent = styled.div`
  max-width: 52%;
  min-width: 52%;
  >h2 {
    margin: 20px 0;
    font-weight: 500;
    font-size: ${({ isBigHeader, isSmallHeader }) => {
    if (isBigHeader) return '48px';
    if (isSmallHeader) return '24px';
    return '36px';
  }};
    @media (max-width: 700px) {
      margin-bottom: 10px;
      font-size: ${({ isBigHeader, isSmallHeader }) => {
    if (isBigHeader) return '36px';
    if (isSmallHeader) return '18px';
    return '24px';
  }};
    }
  }
  >h4 {
    margin: 20px 0;
    font-size: ${({ isPage }) => (isPage ? '18px' : '24px')};
    @media (max-width: 700px) {
      margin: 10px 0;
      font-size: ${({ isPage }) => (isPage ? '13px' : '15px')};
    }
  }
  box-sizing: border-box;
  padding: ${({ isImageFirst }) => (isImageFirst ? '0 0 0 28px' : '0 28px 0 0 ')};
  @media (max-width: 700px) {
    max-width: 100%;
    min-width: 100%;
    text-align: center;
    padding: 0;
  }
`;

const LinkContainer = styled.p`
  margin: 20px 0 0;
  text-align: ${({ isImageFirst }) => (isImageFirst ? 'left' : 'right')};
  >a {
    font-size: 18px;
    font-weight: 500;
    color: ${colors.green};
  }
  @media (max-width: 700px) {
    text-align: center;
  }
`;

const ItemImage = styled.img.attrs({
  src: ({ src }) => src,
})`
  display: block;
  width: 100%;
  height: auto;
`;

const FeatureItem = props => {
  const { isImageFirst, image, name, description, link, isBigHeader, isSmallHeader, isPage, isStaticImage } = props;
  return (
    <MainContainer>
      <ItemContainer isImageFirst={isImageFirst}>
        <ImageContaier>
          <ItemImage src={isStaticImage ? image : getPath(image)} />
        </ImageContaier>
        <ItemContent isImageFirst={isImageFirst} isBigHeader={isBigHeader} isSmallHeader={isSmallHeader} isPage={isPage}>
          <h2>{name}</h2>
          <h4>{description}</h4>
          {!isPage &&
            <LinkContainer isImageFirst={isImageFirst}>
              {isImageFirst ?
                <a href={link} target="_blank">View more <Arrow>←</Arrow></a>
                :
                <a href={link} target="_blank"><Arrow>→</Arrow> View more</a>
              }
            </LinkContainer>
          }
        </ItemContent>
      </ItemContainer>
    </MainContainer>
  );
}

export default FeatureItem;
