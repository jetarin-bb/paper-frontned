import React, { Component } from 'react';
import FeatureItem from '~/components/Home/FeatureItem';

// const data = [{
//   name: 'Production 1',
//   description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at sapien ligula. Aliquam sed orci arcu. Fusce ut ullamcorper nulla.',
//   image: '',
//   link: '/product/ggwp',
//   is_image_first: false,
// }, {
//   name: 'Production 1',
//   description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at sapien ligula. Aliquam sed orci arcu. Fusce ut ullamcorper nulla.',
//   image: '',
//   link: '/product/ggwp',
//   is_image_first: true,
// }, {
//   name: 'Production 1',
//   description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at sapien ligula. Aliquam sed orci arcu. Fusce ut ullamcorper nulla.',
//   image: '',
//   link: '/product/ggwp',
//   is_image_first: false,
// }];

export default class HomeSections extends Component {
  render() {
    const { featureProducts } = this.props;
    return (
      <div>
        {featureProducts && featureProducts.map(item => (
          <FeatureItem
            key={item._id}
            name={item.name}
            description={item.description}
            image={item.image}
            link={item.link}
            isImageFirst={item.is_image_first}
          />
        ))}
      </div>
    );
  }
}
