import React, { Component } from 'react';
import styled from 'styled-components';
import colors from '~/theme/colors';
import api from '~/utils/api';

const Header = styled.h2`
  font-weight: 500;
  font-size: ${({ isPage }) => (isPage ? '36px' : '48px')};
  margin-top: ${({ isPage }) => (isPage ? '20px' : '30px')};
  @media (max-width: 700px) {
    font-size: 24px;
  }
  margin-bottom: 10px;
`;

const SubTitle = styled.h4`
  margin-top: 10px;
  max-width: 500px;
  >a {
    color: ${colors.green};
  }
`;

const InputGroup = styled.div`
  &&::after { 
    content: " ";
    display: block; 
    height: 0; 
    clear: both;
  }
`;
const TextInput = styled.div`
  float: left;
  box-sizing: border-box;
  width: ${({ isHalf }) => (isHalf ? '50%' : '100%')};
  margin-bottom: 20px;
  color: ${colors.lightGray};
  @media (min-width: 701px) {
    &:first-child {
      padding-right: ${({ isHalf }) => (isHalf ? '10px' : '0')};
    }
    &:last-child {
      padding-left: ${({ isHalf }) => (isHalf ? '10px' : '0')};
    }
  }
  @media (max-width: 700px) {
    width: 100%;
    margin-bottom: 10px;
  }
  >textarea {
    display: block;
    width: 100%;
    outline: none;
    box-sizing: border-box;
    border: 1px solid ${colors.borderGray};
    resize: none;
    padding-left: 10px;
    padding-top: 20px;
    font-size: 18px;
    height: 150px;
    @media (max-width: 700px) {
      height: 90px;
      font-size: 13px;
    }
  }
  >input {
    display: block;
    height: 65px;
    width: 100%;
    border: 1px solid ${colors.borderGray};
    font-size: 18px;
    padding-left: 10px;
    outline: none;
    box-sizing: border-box;
    @media (max-width: 700px) {
      height: 45px;
      font-size: 13px;
    }
  }
`;
const SubmitButton = styled.button.attrs({
  type: 'submit',
})`
  display: block;
  margin: 30px auto;
  width: 200px;
  cursor: pointer;
  background-color: ${colors.green};
  color: ${colors.white};
  font-size: 24px;
  border: none;
  box-sizing: border-box;
  height: 65px;
  font-weight: 500;
  @media (max-width: 700px) {
    font-size: 18px;
    height: 45px;
  }
`;
const Sent = styled.h4`
  color: ${colors.darkGreen};
  text-align: center;
  margin: 0;
`;

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      isSent: false,
      isSending: false,
    };
  }
  onSubmit(e) {
    e.preventDefault();
    if (this.state.isSending) return;
    this.setState({ isSending: true })
    api.post('/contactForms', {
      title: this.title && this.title.value,
      name: this.name && this.name.value,
      email: this.email && this.email.value,
      message: this.message && this.message.value,
      order: this.props && this.props.order,
    })
      .then(() => {
        this.setState({ isSent: true, isSending: false });
      });
  }

  render() {
    const { isPage, isHideTitle, header = 'Get in Touch' } = this.props;
    const { isSent } = this.state;
    return (
      <div>
        <Header isPage={isPage} >{header}</Header>
        <SubTitle>
          KUANG TAI SPECIAL PAPER CO.,LTD.
          <br/>
          21/1 MOO 13 SRISATCHANALAI ROAD,
          <br/>
          PAKOOMKOH, SAWANKALOK SUKHOTHAI 64110, THAILAND
          <br/>
          TEL: 66-55-643085, FAX: 66-55-641577
          <br/>
          EMAIL: <a href="mailto:info@ktsinfo.com">info@ktsinfo.com</a>
        </SubTitle>
        <form onSubmit={this.onSubmit}>
          {!isHideTitle &&
          <InputGroup>
            <TextInput>
              <input type="text" placeholder="Title" required ref={(input) => { this.title = input; }} disabled={isSent} />
            </TextInput>
          </InputGroup>
          }
          <InputGroup>
            <TextInput isHalf>
              <input type="text" placeholder="Name" required ref={(input) => { this.name = input; }} disabled={isSent} />
            </TextInput>
            <TextInput isHalf>
              <input type="email" placeholder="Email" required ref={(input) => { this.email = input; }} disabled={isSent} />
            </TextInput>
          </InputGroup>
          <InputGroup>
            <TextInput>
              <textarea placeholder="Message" required ref={(input) => { this.message = input; }} disabled={isSent} />
            </TextInput>
          </InputGroup>
          {isSent ?
            <Sent>
              ✔ Successfuly Sent
            </Sent>
            :
            <SubmitButton>Send</SubmitButton>
          }
        </form>
      </div>
    );
  }
}

