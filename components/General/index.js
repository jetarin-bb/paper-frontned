import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import colors from '~/theme/colors';

export const Inner = styled.div`
  margin: 0 auto;
  padding: 20px 148px;
  max-width: 1280px;
  box-sizing: border-box;
  @media (max-width: 700px) {
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
  }
`;

const BreadCrumbContainer = styled.div`
  margin: 10px 0;
`;
const BreadCrumbLink = styled.span`
  >a {
    color: ${colors.gray};
    font-size: 15px;
  }
  &:last-child {
    >a {
      color: ${colors.green};
    }
  }
  &:not(:last-child) {
    &::after {
      content: ' / ';
    }
  }
`;
const Chevron = styled.span`
  font-size: 30px;
  position: relative;
  top: 3px;
  color: ${colors.lightGray};
`;

export const BreadCrumb = ({ data }) => (
  <BreadCrumbContainer>
    <Chevron>&lt;</Chevron>
    {data.map((item, index) => (
      <BreadCrumbLink key={`${item.link}-${index}`}>
        <Link href={item.link}><a>{item.label}</a></Link>
      </BreadCrumbLink>
    ))}
  </BreadCrumbContainer>
);

const CustomSelect = styled.select`
  height: 45px;
  width: ${({ width }) => width || '100px'};
  color: ${colors.gray};
  background-color: ${colors.white};
  border: 1px solid ${colors.borderGray};
  font-size: 18px;
  padding-top: 3px;
  padding-left: 10px;
  @media (max-width: 700px) {
    height: 35px;
    font-size: 15px;
    padding-top: 0;
  }
`;

export const Select = props => {
  const { options, width, value } = props;
  return (
    <CustomSelect onChange={props.onChange} width={width} value={(value !== undefined && value !== null && value) || undefined} >
      {options.map((opt, index) => (
        <option value={opt.value} key={`opt-${index}-${opt.value}`}>{opt.label}</option>
      ))}
    </CustomSelect>
  );
};

export default {};
