import React, { Component } from 'react';

import { Inner, BreadCrumb } from '~/components/General';
import ProductInfo from '~/components/ProductDetail/ProductInfo';
import RelatedProducts from '~/components/ProductDetail/RelatedProducts';
import connect from '~/redux/withRedux';
import { actions as paperActions } from '~/redux/reducers/paper';
import styled from 'styled-components';
import Form from '~/components/Form';
import { Element } from 'react-scroll';
import { Helmet } from 'react-helmet';

const MainContainer = styled.div`
  @media (max-width: 700px) {
    padding-top: 50px;
  }
`;

const MobileHide = styled.div`
  @media (max-width: 700px) {
    display: none;
  }
`;

@connect(
  state => ({
    paperDetail: state.paper.paperDetail,
  }),
  {
    ...paperActions,
  }
)
export default class PaperDetail extends Component {
  static async getInitialProps({ store, query }) {
    try {
      await store.dispatch(paperActions.getPaperDetail(query.slug));
    } catch (err) {
      console.log('ERROR: ', err);
    }
    return {};
  }
  render() {
    const data = this.props.paperDetail;
    const breadcrumb = [{
      label: 'Home',
      link: '/',
    }, {
      label: 'All Paper',
      link: '/paper'
    }];
    if (data.type_id && data.type_id.name) {
      breadcrumb.push({ label: data.type_id.name, link: `/paper?type=${data.type_id.name}`});
    }
    breadcrumb.push({ label: data.name, link: `/paper/${data.slug}`});
    return (
      <MainContainer>
        <Helmet>
          <title>{data.name} | </title>
        </Helmet>
        <Inner>
          <MobileHide>
            <BreadCrumb data={breadcrumb} />
          </MobileHide>
          <ProductInfo
            name={data.name}
            type={data.type_id && data.type_id.name}
            description={data.description}
            spec={data.spec}
            images={data.images}
            availableColors={data.available_colors}
          />
          <RelatedProducts
            products={data.related_paper_ids}
            path="paper"
          />
          <Element name="order">
            <Form header="Order this paper" order={data.name} />
          </Element>
        </Inner>
      </MainContainer>
    );
  }
}


