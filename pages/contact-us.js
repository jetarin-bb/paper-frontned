import React, { Component } from 'react';
// import Link from 'next/link';
// import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
// import Router from 'next/router';

import connect from '~/redux/withRedux';
// import { actions as productActions } from '~/redux/reducers/product';
import { Inner } from '~/components/General';
import Form from '~/components/Form';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';


const FormContainer = styled.div`
  @media (max-width: 700px) {
    margin-top: 50px;
  }
`;

@connect(
  () => ({
  }),
  {
    // ...productActions,
  }
)
export default class ContactUs extends Component {
  // static async getInitialProps({ store, query }) {
  //   try {
  //     await store.dispatch(productActions.getProducts());
  //   } catch (err) {
  //     console.log('ERROR: ', err);
  //   }
  //   return {};
  // }
  render() {
    return (
      <div>
        <Helmet>
          <title>Contact Us | </title>
        </Helmet>
        <Inner>
          <FormContainer>
            <Form isPage />
          </FormContainer>
        </Inner>
      </div>
    );
  }
}
