import React, { Component } from 'react';
import Link from 'next/link'
import Router from 'next/router'
// import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
// import Router from 'next/router';

import { Inner, BreadCrumb } from '~/components/General';
import ProductFilter from '~/components/Product/ProductFilter';
import ProductNav from '~/components/Product/ProductNav';
import ProductBox from '~/components/Product/ProductBox';
import connect from '~/redux/withRedux';
import { actions as productActions } from '~/redux/reducers/product';
import styled from 'styled-components';
import colors from '~/theme/colors';
import { Helmet } from 'react-helmet';

const MainContainer = styled.div`
  @media (max-width: 700px) {
    padding-top: 50px;
  }
`;

const MobileHide = styled.div`
  @media (max-width: 700px) {
    display: none;
  }
`;
const BottomFilter = styled.div`
  text-align: right;
  @media (max-width: 700px) {
    text-align: center;
  }
`;
const ProductsContainer = styled.div`
  margin: 20px 0;
  &::after{
    content: " ";
    display: block; 
    height: 0; 
    clear: both;
  }
  border-bottom: 1px solid ${colors.lineGray};
`;

const ProductBoxContainer = styled.div`
  float: left;
  width: 33.33%;
  box-sizing: border-box;
  @media (min-width: 701px) {
    &:nth-child(3n+1) {
      padding-right: 13.34px;
      clear: both;
    }
    &:nth-child(3n+2) {
      padding-left: 6.66px;
      padding-right: 6.66px;
    }
    &:nth-child(3n+3) {
      padding-left: 13.34px;
    }
    margin-bottom: 20px;
  }
  @media (max-width: 700px) {
  ${({ view }) => (view === 'box' ?
    `
      width: 50%;
      &:nth-child(2n+1) {
        padding-right: 5px;
        clear: both;
      }
      &:nth-child(2n+2) {
        padding-left: 5px;
      }
    `
    :
    `
      width: 100%;
      clear: both;
      &:not(:last-child) {
        border-bottom: 1px solid ${colors.lineGray};
      }
      &:not(:first-child) {
        margin-top: 10px;
        border-bottom: 1px solid ${colors.lineGray};
      }
      padding-bottom: 10px;
    `
  )}
  }
`;

@connect(
  state => ({
    products: state.product.products,
    isLoading: state.product.isLoading,
    selectedProductType: state.product.selectedProductType,
    productTypes: state.product.productTypes,
    total: state.product.total,
    product: state.product.products,
    currentPage: state.product.currentPage,
    //
    count: state.product.count,
  }),
  {
    ...productActions,
  }
)
export default class Products extends Component {
  static async getInitialProps({ store, query }) {
    try {
      const types = await store.dispatch(productActions.getProductTypes());
      const selectedType = (types.data && types.data.map(t => t.name).indexOf(query.type) >= 0 && query.type) || '';
      store.dispatch(productActions.selectProductType(selectedType));
      store.dispatch(productActions.changeCurrentPage(1));
      await store.dispatch(productActions.getProducts(selectedType));
    } catch (err) {
      console.log('ERROR: ', err);
    }
    return {};
  }
  constructor(props) {
    super(props);
    this.getBreadcrumb = this.getBreadcrumb.bind(this);
    this.renderNav = this.renderNav.bind(this);
    this.state = {
      view: 'box',
    };
  }
  getBreadcrumb() {
    const { selectedProductType } = this.props;
    const breadcrumb = [{
      label: 'Home',
      link: '/',
    }, {
      label: 'All Product',
      link: '/products'
    }];
    if (selectedProductType) {
      breadcrumb.push({ label: selectedProductType, link: `/products?type=${selectedProductType}` });
    }
    return breadcrumb;
  }
  renderNav() {
    return (
      <ProductNav
        pages={Math.ceil(this.props.total / 9)}
        currentPage={this.props.currentPage}
        onChangePage={page => {
          this.props.changeCurrentPage(page);
          this.props.getProducts(this.props.selectedProductType, (page - 1) * 9);
          if (window) {
            window.scrollTo(0, 0);
          }
        }}
      />
    );
  }
  render() {
    const { products, productTypes, selectedProductType, total } = this.props;
    return (
      <MainContainer>
        <Helmet>
          <title>Products | </title>
        </Helmet>
        <Inner>
          <MobileHide>
            <BreadCrumb data={this.getBreadcrumb()} />
          </MobileHide>
          <ProductFilter
            view={this.state.view}
            onChangeView={view => this.setState({ view })}
            productTypes={[{ label: 'All Products', value: '' }, ...productTypes.map(pt => ({ label: pt, value: pt }))]}
            selectedProductType={selectedProductType}
            showing={products.length}
            total={total}
            onChangeProductType={type => {
              Router.push({
                pathname: '/products',
                query: { type }
              });
            }}
            renderNav={this.renderNav}
          />
          <ProductsContainer>
            {products.map((item, index) => (
              <ProductBoxContainer key={`item-${index}`} view={this.state.view}>
                <ProductBox
                  name={item.name}
                  description={item.description}
                  slug={item.slug}
                  type={item.type}
                  coverImage={item.cover_image}
                  view={this.state.view}
                  path="product"
                />
              </ProductBoxContainer>
            ))}
          </ProductsContainer>
          <BottomFilter>
            {this.renderNav()}
          </BottomFilter>
        </Inner>
      </MainContainer>
    );
  }
}
