import React, { Component } from 'react';
// import Link from 'next/link';
// import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
// import Router from 'next/router';

import connect from '~/redux/withRedux';
import { actions as homeActions } from '~/redux/reducers/home';
import { Inner } from '~/components/General';
import HomeSections from '~/components/Home/HomeSections';
import FeatureItem from '~/components/Home/FeatureItem';
import HomeSlider from '~/components/Home/HomeSlider';
import Form from '~/components/Form';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

// const ReloadButton = styled.a`
//   color: gray;
//   padding: 4px 6px;
//   border-radius: 5px;
//   background-color: coral;
// `;

// const CountButton = styled.a`
//   color: red;
//   padding: 4px 6px;
//   border-radius: 5px;
//   background-color: green;
// `;

const FormContainer = styled.div`
  margin-top: 100px;
  @media (max-width: 700px) {
    margin-top: 50px;
  }
`;

@connect(
  state => ({
    featureProducts: state.home.featureProducts,
    sliderProducts: state.home.sliderProducts,
  }),
  {}
)
export default class Home extends Component {
  static async getInitialProps({ store }) {
    try {
      await Promise.all([
        store.dispatch(homeActions.getFeatureProducts()),
        store.dispatch(homeActions.getSliderProducts()),
      ]);
    } catch (err) {
      console.log('ERROR: ', err);
    }
    return {};
  }
  render() {
    const { featureProducts, sliderProducts } = this.props;
    const aboutUs = {
      name: 'About us',
      description: 'KUANG TAI SPECIAL PAPER began our business as a crops merchandising in 1977 and export to Japan and Taiwan market. In 1985, we start up our Mulberry paper business as a manufacturer and exporter to Asia and expand to Europe and USA market consequently, developing to one of the leading company of Mulberry papers business worldwide.',
      link: '/about-us',
      image: '/static/images/about-us/about-us1.jpg',
      isStaticImage: true,
      isImageFirst: true,
    };
    return (
      <div>
        <Inner>
          <HomeSections featureProducts={featureProducts} />
          <HomeSlider items={sliderProducts} />
          <FeatureItem {...aboutUs} isBigHeader />
          <FormContainer>
            <Form isHideTitle />
          </FormContainer>
        </Inner>
      </div>
    );
  }
}

