import React, { Component } from 'react';
import Router from 'next/router'

import { Inner, BreadCrumb } from '~/components/General';
import ProductFilter from '~/components/Product/ProductFilter';
import ProductNav from '~/components/Product/ProductNav';
import ProductBox from '~/components/Product/ProductBox';
import connect from '~/redux/withRedux';
import { actions as paperActions } from '~/redux/reducers/paper';
import styled from 'styled-components';
import colors from '~/theme/colors';
import { Helmet } from 'react-helmet';

const MainContainer = styled.div`
  @media (max-width: 700px) {
    padding-top: 50px;
  }
`;

const MobileHide = styled.div`
  @media (max-width: 700px) {
    display: none;
  }
`;
const BottomFilter = styled.div`
  text-align: right;
  @media (max-width: 700px) {
    text-align: center;
  }
`;
const ProductsContainer = styled.div`
  margin: 20px 0;
  &::after{
    content: " ";
    display: block; 
    height: 0; 
    clear: both;
  }
  border-bottom: 1px solid ${colors.lineGray};
`;

const ProductBoxContainer = styled.div`
  float: left;
  width: 33.33%;
  box-sizing: border-box;
  @media (min-width: 701px) {
    &:nth-child(3n+1) {
      padding-right: 13.34px;
      clear: both;
    }
    &:nth-child(3n+2) {
      padding-left: 6.66px;
      padding-right: 6.66px;
    }
    &:nth-child(3n+3) {
      padding-left: 13.34px;
    }
    margin-bottom: 20px;
  }
  @media (max-width: 700px) {
  ${({ view }) => (view === 'box' ?
    `
      width: 50%;
      &:nth-child(2n+1) {
        padding-right: 5px;
        clear: both;
      }
      &:nth-child(2n+2) {
        padding-left: 5px;
      }
    `
    :
    `
      width: 100%;
      clear: both;
      &:not(:last-child) {
        border-bottom: 1px solid ${colors.lineGray};
      }
      &:not(:first-child) {
        margin-top: 10px;
        border-bottom: 1px solid ${colors.lineGray};
      }
      padding-bottom: 10px;
    `
  )}
  }
`;

@connect(
  state => ({
    paper: state.paper.paper,
    selectedPaperType: state.paper.selectedPaperType,
    paperTypes: state.paper.paperTypes,
    total: state.paper.total,
    currentPage: state.paper.currentPage,
  }),
  {
    ...paperActions,
  }
)
export default class Paper extends Component {
  static async getInitialProps({ store, query }) {
    try {
      const types = await store.dispatch(paperActions.getPaperTypes());
      const selectedType = (types.data && types.data.map(t => t.name).indexOf(query.type) >= 0 && query.type) || '';
      store.dispatch(paperActions.selectPaperType(selectedType));
      store.dispatch(paperActions.changeCurrentPage(1));
      await store.dispatch(paperActions.getPaper(selectedType));
    } catch (err) {
      console.log('ERROR: ', err);
    }
    return {};
  }
  constructor(props) {
    super(props);
    this.getBreadcrumb = this.getBreadcrumb.bind(this);
    this.renderNav = this.renderNav.bind(this);
    this.state = {
      view: 'box',
    };
  }
  getBreadcrumb() {
    const { selectedPaperType } = this.props;
    const breadcrumb = [{
      label: 'Home',
      link: '/',
    }, {
      label: 'All Paper',
      link: '/paper'
    }];
    if (selectedPaperType) {
      breadcrumb.push({ label: selectedPaperType, link: `/paper?type=${selectedPaperType}` });
    }
    return breadcrumb;
  }
  renderNav() {
    return (
      <ProductNav
        pages={Math.ceil(this.props.total / 9)}
        currentPage={this.props.currentPage}
        onChangePage={page => {
          this.props.changeCurrentPage(page);
          this.props.getPaper(this.props.selectedPaperType, (page - 1) * 9);
          if (window) {
            window.scrollTo(0, 0);
          }
        }}
      />
    );
  }
  render() {
    const { paper, paperTypes, selectedPaperType, total } = this.props;
    return (
      <MainContainer>
        <Helmet>
          <title>Paper | </title>
        </Helmet>
        <Inner>
          <MobileHide>
            <BreadCrumb data={this.getBreadcrumb()} />
          </MobileHide>
          <ProductFilter
            view={this.state.view}
            onChangeView={view => this.setState({ view })}
            productTypes={[{ label: 'All Paper', value: '' }, ...paperTypes.map(pt => ({ label: pt, value: pt }))]}
            selectedProductType={selectedPaperType}
            showing={paper.length}
            total={total}
            onChangeProductType={type => {
              Router.push({
                pathname: '/paper',
                query: { type }
              });
            }}
            renderNav={this.renderNav}
          />
          <ProductsContainer>
            {paper.map((item, index) => (
              <ProductBoxContainer key={`item-${index}`} view={this.state.view}>
                <ProductBox
                  name={item.name}
                  description={item.description}
                  slug={item.slug}
                  type={item.type}
                  coverImage={item.cover_image}
                  view={this.state.view}
                  path="paper"
                />
              </ProductBoxContainer>
            ))}
          </ProductsContainer>
          <BottomFilter>
            {this.renderNav()}
          </BottomFilter>
        </Inner>
      </MainContainer>
    );
  }
}

