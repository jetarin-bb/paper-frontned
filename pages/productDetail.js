import React, { Component } from 'react';

import { Inner, BreadCrumb } from '~/components/General';
import ProductInfo from '~/components/ProductDetail/ProductInfo';
import RelatedProducts from '~/components/ProductDetail/RelatedProducts';
import connect from '~/redux/withRedux';
import { actions as productActions } from '~/redux/reducers/product';
import styled from 'styled-components';
import Form from '~/components/Form';
import { Element } from 'react-scroll'
import { Helmet } from 'react-helmet';

const MainContainer = styled.div`
  @media (max-width: 700px) {
    padding-top: 50px;
  }
`;

const MobileHide = styled.div`
  @media (max-width: 700px) {
    display: none;
  }
`;

@connect(
  state => ({
    productDetail: state.product.productDetail,
  }),
  {
    ...productActions,
  }
)
export default class ProductDetail extends Component {
  static async getInitialProps({ store, query }) {
    try {
      await store.dispatch(productActions.getProductDetail(query.slug));
    } catch (err) {
      console.log('ERROR: ', err);
    }
    return {};
  }
  render() {
    const data = this.props.productDetail;
    const breadcrumb = [{
      label: 'Home',
      link: '/',
    }, {
      label: 'All Product',
      link: '/products'
    }];
    if (data.type_id && data.type_id.name) {
      breadcrumb.push({ label: data.type_id.name, link: `/products?type=${data.type_id.name}`});
    }
    breadcrumb.push({ label: data.name, link: `/product/${data.slug}`});
    return (
      <MainContainer>
        <Helmet>
          <title>{data.name} | </title>
        </Helmet>
        <Inner>
          <MobileHide>
            <BreadCrumb data={breadcrumb} />
          </MobileHide>
          <ProductInfo
            name={data.name}
            type={data.type_id && data.type_id.name}
            description={data.description}
            spec={data.spec}
            images={data.images}
            availableColors={data.available_colors}
          />
          <RelatedProducts
            products={data.related_product_ids}
          />
          <Element name="order">
            <Form header="Order this product" order={data.name} />
          </Element>
        </Inner>
      </MainContainer>
    );
  }
}

