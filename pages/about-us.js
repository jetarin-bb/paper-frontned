import React, { Component } from 'react';
import Link from 'next/link';
// import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
// import Router from 'next/router';

import connect from '~/redux/withRedux';
// import { actions as productActions } from '~/redux/reducers/product';
import { Inner } from '~/components/General';
import FeatureItem from '~/components/Home/FeatureItem';
import styled from 'styled-components';
import colors from '~/theme/colors';
import { Helmet } from 'react-helmet';

const Container = styled.div`
  @media (max-width: 700px) {
    margin-top: 60px;
  }
`;

const ContentContainer = styled.div`
  margin-top: 20px;
  padding-top: 10px;
  border-top: 1px solid ${colors.lineGray};
  text-align: center;
  >h2 {
    font-size: 36px;
    @media (max-width: 700px) {
      font-size: 24px;
    }
    margin: 10px auto;
    max-width: 600px;
  }
  >p {
    margin: 10px auto;
    max-width: 600px;
  }
`;
const Button = styled.div`
  >a {
    display: block;
    margin: 30px auto;
    width: 200px;
    background-color: ${colors.green};
    color: ${colors.white};
    font-size: 24px;
    border: none;
    box-sizing: border-box;
    height: 65px;
    font-weight: 500;
    padding-top: 16px;
    @media (max-width: 700px) {
      font-size: 18px;
      height: 45px;
      padding-top: 11px;
    }
  }
`;
@connect(
  () => ({
  }),
  {
    // ...productActions,
  }
)
export default class AboutUs extends Component {
  // static async getInitialProps({ store, query }) {
  //   try {
  //     await store.dispatch(productActions.getProducts());
  //   } catch (err) {
  //     console.log('ERROR: ', err);
  //   }
  //   return {};
  // }
  render() {
    const aboutUs = {
      name: 'About us',
      description: 'KUANG TAI SPECIAL PAPER began our business as a crops merchandising in 1977 and export to Japan and Taiwan market. In 1985, we start up our Mulberry paper business as a manufacturer and exporter to Asia and expand to Europe and USA market consequently, developing to one of the leading company of Mulberry papers business worldwide.',
      link: '/about-us',
      image: '/static/images/about-us/about-us1.jpg',
      isStaticImage: true,
      isImageFirst: true,
    };
    const aboutUs2 = {
      description: 'Over 33 years experience in Mulberry paper business, we have focused and continued to offer the best quality, on-time delivery and variety of our papers and products. We works directly with designers and artists to create new ideas into gorgeous papers. Having been located in Sukhothai province which rich of natural plants, we are the pioneer who take real plants to make several beautiful natural papers such as Banana paper, Mango paper, Sugarcane paper, and more Eco-friendly papers.',
      image: '/static/images/about-us/about-us2.jpg',
      isStaticImage: true,
    };
    const aboutUs3 = {
      description: 'We do much more than just making unique papers! Our proud of a long contribution to create jobs for our community is a mission to let the local people be able to earn their living as well as stay with their family.',
      isImageFirst: true,
      image: '/static/images/about-us/about-us3.jpg',
      isStaticImage: true,
    };
    return (
      <div>
        <Helmet>
          <title>About Us | </title>
        </Helmet>
        <Inner>
          <Container>
            <FeatureItem {...aboutUs} isPage />
            <FeatureItem {...aboutUs2} isSmallHeader isPage />
            <FeatureItem {...aboutUs3} isSmallHeader isPage />
            <ContentContainer>
              <h2>Do you need good Product?</h2>
              <p>
                Get in touch with us. We’re proud to share our creativity and passion of Mulberry paper.
              </p>
              <Button>
                <Link href="/contact-us">Go</Link>
              </Button>
            </ContentContainer>
          </Container>
        </Inner>
      </div>
    );
  }
}

