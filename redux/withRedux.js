import React from 'react';
// import PropTypes from 'prop-types';
import withRedux from 'next-redux-wrapper';
import createStore from '~/redux/createStore';
import App from '~/components/App';
import GlobalStyle from '~/theme/GlobalStyle';
import SlickStyle from '~/theme/SlickStyle';
// import { actions as infoActions } from '~/redux/reducers/info';

const _withRedux = (mapStateToProps, mapDispatchToProps) => C => {
  const Layout = props => (
    <App {...props}>
      <C {...props} />
      <GlobalStyle />
      <SlickStyle />
    </App>
  );
  Layout.displayName = 'Layout';
  // Layout.propTypes = { url: PropTypes.object };
  Layout.getInitialProps = async (data) => {
    // const { store } = data;
    // const state = store.getState();
    // if (!state.info.combinedStaticLoaded) {
    //   await store.dispatch(infoActions.loadCombinedStatic());
    // }
    if (C.getInitialProps) {
      await C.getInitialProps(data);
    }
  };

  const ConnectedComponent = withRedux(
    createStore,
    mapStateToProps,
    mapDispatchToProps
  )(Layout);
  return ConnectedComponent;
};

export default _withRedux;
