import { constantCreator } from '~/utils/constantsCreator';
import api from '~/utils/api';

const constant = constantCreator('paper');
const GET_PAPER = constant('GET_PAPER', true);
const GET_PAPER_TYPES = constant('GET_PAPER_TYPES', true);
const SELECT_PAPER_TYPE = constant('SELECT_PAPER_TYPE');
const CHANGE_CURRENT_PAGE = constant('SELECT_CURRENT_PAGE');
const GET_PAPER_DETAIL = constant('GET_PAPER_DETAIL', true);

const initialState = {
  isLoading: true,
  isError: false,
  paper: [],
  paperTypes: [],
  selectedPaperType: '',
  total: 0,
  currentPage: 1,
  paperDetail: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PAPER.PENDING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case GET_PAPER.RESOLVED:
      return {
        ...state,
        isLoading: false,
        isError: false,
        paper: action.data.paper,
        total: action.data.total,
      };
    case GET_PAPER.REJECTED:
      return {
        ...state,
        isLoading: false,
        isError: true,
        error: action.error,
      };
    case GET_PAPER_TYPES.RESOLVED:
      return {
        ...state,
        paperTypes: action.data && action.data.map(t => t.name),
      };
    case SELECT_PAPER_TYPE:
      return {
        ...state,
        selectedPaperType: action.selectedPaperType,
      };
    case CHANGE_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.currentPage,
      };
    case GET_PAPER_DETAIL.RESOLVED:
      return {
        ...state,
        paperDetail: action.data,
      };
    default:
      return state;
  }
};

export default reducer;

export const actions = {
  getPaper: (type, skip = 0, limit = 9) => ({
    type: GET_PAPER,
    promise: api.get(`/paper/criteria?skip=${skip}&limit=${limit}${type ? `&type=${type}` : ''}`),
  }),
  getPaperTypes: () => ({
    type: GET_PAPER_TYPES,
    promise: api.get('/paperTypes'),
  }),
  selectPaperType: (selectedPaperType) => ({
    type: SELECT_PAPER_TYPE,
    selectedPaperType,
  }),
  changeCurrentPage: (currentPage) => ({
    type: CHANGE_CURRENT_PAGE,
    currentPage,
  }),
  getPaperDetail: (slug) => ({
    type: GET_PAPER_DETAIL,
    promise: api.get(`/paper/slug/${slug}`),
  }),
};
