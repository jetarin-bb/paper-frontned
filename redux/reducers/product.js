import { constantCreator } from '~/utils/constantsCreator';
import api from '~/utils/api';

const constant = constantCreator('product');
const GET_PRODUCTS = constant('GET_PRODUCTS', true);
const GET_PRODUCT_TYPES = constant('GET_PRODUCT_TYPES', true);
const SELECT_PRODUCT_TYPE = constant('SELECT_PRODUCT_TYPE');
const CHANGE_CURRENT_PAGE = constant('SELECT_CURRENT_PAGE');
const GET_PRODUCT_DETAIL = constant('GET_PRODUCT_DETAIL', true);

const initialState = {
  isLoading: true,
  isError: false,
  products: [],
  productTypes: [],
  selectedProductType: '',
  total: 0,
  currentPage: 1,
  productDetail: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS.PENDING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case GET_PRODUCTS.RESOLVED:
      return {
        ...state,
        isLoading: false,
        isError: false,
        products: action.data.products,
        total: action.data.total,
      };
    case GET_PRODUCTS.REJECTED:
      return {
        ...state,
        isLoading: false,
        isError: true,
        error: action.error,
      };
    case GET_PRODUCT_TYPES.RESOLVED:
      return {
        ...state,
        productTypes: action.data && action.data.map(t => t.name),
      };
    case SELECT_PRODUCT_TYPE:
      return {
        ...state,
        selectedProductType: action.selectedProductType,
      };
    case CHANGE_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.currentPage,
      };
    case GET_PRODUCT_DETAIL.RESOLVED:
      return {
        ...state,
        productDetail: action.data,
      };
    default:
      return state;
  }
};

export default reducer;

export const actions = {
  getProducts: (type, skip = 0, limit = 9) => ({
    type: GET_PRODUCTS,
    promise: api.get(`/products/criteria?skip=${skip}&limit=${limit}${type ? `&type=${type}` : ''}`),
  }),
  getProductTypes: () => ({
    type: GET_PRODUCT_TYPES,
    promise: api.get('/productTypes'),
  }),
  selectProductType: (selectedProductType) => ({
    type: SELECT_PRODUCT_TYPE,
    selectedProductType,
  }),
  changeCurrentPage: (currentPage) => ({
    type: CHANGE_CURRENT_PAGE,
    currentPage,
  }),
  getProductDetail: (slug) => ({
    type: GET_PRODUCT_DETAIL,
    promise: api.get(`/products/slug/${slug}`),
  }),
};
