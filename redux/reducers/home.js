import { constantCreator } from '~/utils/constantsCreator';
import api from '~/utils/api';

const constant = constantCreator('home');
const GET_FEATURE_PRODUCTS = constant('GET_FEATURE_PRODUCTS', true);
const GET_FEATURE_PAPER = constant('GET_FEATURE_PAPER', true);
const GET_SLIDER_PRODUCTS = constant('GET_SLIDER_PRODUCTS', true);

const initialState = {
  featureProducts: [],
  featurePaper: [],
  sliderProducts: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FEATURE_PRODUCTS.RESOLVED:
      return {
        ...state,
        featureProducts: action.data,
      };
    case GET_FEATURE_PAPER.RESOLVED:
      return {
        ...state,
        featurePaper: action.data
      };
    case GET_SLIDER_PRODUCTS.RESOLVED:
      return {
        ...state,
        sliderProducts: action.data,
      };
    default:
      return state;
  }
};

export default reducer;

export const actions = {
  getFeatureProducts: () => ({
    type: GET_FEATURE_PRODUCTS,
    promise: api.get('/featureProducts'),
  }),
  getFeaturePaper: () => ({
    type: GET_FEATURE_PAPER,
    promise: api.get('/featurePaper'),
  }),
  getSliderProducts: () => ({
    type: GET_SLIDER_PRODUCTS,
    promise: api.get('/sliderProducts'),
  }),
};
