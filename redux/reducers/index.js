import { combineReducers } from 'redux';

import app from './app';
import home from './home';
import product from './product';
import paper from './paper';

export default combineReducers({
  app,
  home,
  product,
  paper
});
