import { constantCreator } from '~/utils/constantsCreator';
// import api from '~/utils/api';

const constant = constantCreator('app');

const LOADING = constant('LOADING');
const LOADED = constant('LOADED');
const SET_TYPES = constant('SET_TYPES');

const initialState = {
  isLoading: false,
  isTypesLoaded: false,
  paperTypes: [],
  productTypes: [],
};

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case LOADED:
      return {
        ...state,
        isLoading: false,
      };
    case SET_TYPES:
      return {
        ...state,
        productTypes: action.productTypes,
        paperTypes: action.paperTypes,
        isTypesLoaded: true,
      };
    default:
      return state;
  }
};

export const actions = {
  loading: () => ({ type: LOADING }),
  loaded: () => ({ type: LOADED }),
  setTypes: ({ paperTypes, productTypes }) => ({
    type: SET_TYPES,
    paperTypes,
    productTypes,
  })
};

export default reducer;
