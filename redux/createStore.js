import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from './middlewares/promiseMiddleware';
import reducer from './reducers';

export default initialState => {
  const storeCreator = (process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window && window.devToolsExtension) ?
    compose(
      applyMiddleware(thunkMiddleware, promiseMiddleware),
      window.devToolsExtension()
    )
    :
    compose(applyMiddleware(thunkMiddleware, promiseMiddleware));
  return createStore(
    reducer,
    initialState,
    storeCreator
  );
};
