global.__CLIENT__ = false;
global.__DEV__ =
  process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'staging';

const express = require('express');
const next = require('next');
const proxy = require('express-http-proxy');

const port = parseInt(process.env.PORT, 10) || 3002;
const dev = __DEV__;
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = express();
    server.use('/api', proxy('http://45.32.117.235:3001/'));
    server.get('/', (req, res) => {
      return app.render(req, res, '/', req.query);
    });

    server.get('/product/:slug', (req, res) => {
      return app.render(req, res, '/productDetail', { ...req.query, slug: req.params.slug });
    });

    server.get('/products', (req, res) => {
      return app.render(req, res, '/products', req.query);
    });

    server.get('/paper/:slug', (req, res) => {
      return app.render(req, res, '/paperDetail', { ...req.query, slug: req.params.slug });
    });
    server.get('/paper', (req, res) => {
      return app.render(req, res, '/paper', req.query);
    });
    server.get('*', (req, res) => {
      return handle(req, res);
    });

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  });
