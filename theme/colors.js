const colors = {
  white: '#ffffff',
  cyan: '#f8fcff',
  green: '#98be64',
  gray: '#4a4a4a',
  lightGray: '#9b9b9b',
  footerGray: '#ccc5be',
  borderGray: '#979797',
  darkGreen: '#417505',
  lineGray: '#dcdcdc',
};

export default colors;
