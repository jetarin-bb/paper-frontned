import React from 'react';

const GlobalStyle = () => (
  <style jsx global>
{`
@font-face {
  font-family: 'Leelawadee UI';
  src: url('/static/fonts/LeelaUIsl.otf');
  font-weight: 300;
}
@font-face {
  font-family: 'Leelawadee UI';
  src: url('/static/fonts/LeelaUI.otf');
  font-weight: 400;
}
@font-face {
  font-family: 'Leelawadee UI';
  src: url('/static/fonts/LeelaUIb.otf');
  font-weight: 500;
}

body {
  line-height: 1.3;
  color: #9b9b9b;
  margin: 0;
  font-family: 'Leelawadee UI';
  font-weight: 400;
}
button {
  font-family: 'Leelawadee UI';
}
a {
  text-decoration: none;
  color: #4a4a4a;
}
h1 {
  font-size: 60px;
  color: #4a4a4a;
  font-weight: 500;
}
h2 {
  font-size: 48px;
  color: #4a4a4a;
  font-weight: 400;
}
h3 {
  font-size: 30px;
  color: #4a4a4a;
  font-weight: 400;
}
h4 {
  font-size: 24px;
  font-weight: 400;
}
p {
  font-size: 18px;
  font-weight: 400;
}
.spinner-container {
  position: fixed;
  height: 100%;
  width: 100%;
  background-color: rgba(255, 255, 255, 0.7);
  top: 0;
  left: 0;
  z-index: 200;
}
.spinner {
  margin: 100px auto;
  width: 40px;
  height: 40px;
  text-align: center;
  -webkit-animation: sk-rotate 2.0s infinite linear;
  animation: sk-rotate 2.0s infinite linear;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
}

.dot1, .dot2 {
  width: 60%;
  height: 60%;
  display: inline-block;
  position: absolute;
  top: 0;
  background-color: #98be64;
  border-radius: 100%;
  
  -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
  animation: sk-bounce 2.0s infinite ease-in-out;
}

.dot2 {
  top: auto;
  bottom: 0;
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

@-webkit-keyframes sk-rotate { 100% { -webkit-transform: rotate(360deg) }}
@keyframes sk-rotate { 100% { transform: rotate(360deg); -webkit-transform: rotate(360deg) }}

@-webkit-keyframes sk-bounce {
  0%, 100% { -webkit-transform: scale(0.0) }
  50% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bounce {
  0%, 100% { 
    transform: scale(0.0);
    -webkit-transform: scale(0.0);
  } 50% { 
    transform: scale(1.0);
    -webkit-transform: scale(1.0);
  }
}
@media (max-width: 700px) {
  h1 {
    font-size: 36px;
  }
  h2 {
    font-size: 24px;
  }
  h3 {
    font-size: 18px;
  }
  h4 {
    font-size: 15px;
  }
  p {
    font-size: 13px;
  }
}
`}
  </style>
);

export default GlobalStyle;
